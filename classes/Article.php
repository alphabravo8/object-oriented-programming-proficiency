<?php

abstract class Article implements ArticleInterface
{

    public $id;
    public $Title;
    public $Subtitle;
    public $Excerpt;
    public $Body;
    public $Author;
    public $DateCreated;
    public $DatePublished;
    public $Status;
    public $Type = NULL;

    public function Article( $Title, $Subtitle, $Excerpt, $Body ) {
        $this->Title = $Title;
        $this->Subtitle = $Subtitle;
        $this->Excerpt = $Excerpt;
        $this->Body = $Body;
    }

    public function view($id) {
	// DB -> fetch and return data object
	global $data;
	if (isset($data[$id])) {
        	return $data[$id];
	}
    }

    public function publish(Article $article) {
        return 'publish article';
    }

    public function delete($id) {
        global $data;
	unset($data[$id]);
    }

}
