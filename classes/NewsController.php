<?php

class NewsController {

    public function view() {
        $news = News::view($_GET['id']);
        if (is_a($news, 'News')) {
            require_once('views/news/view.php');
        } else {
            die("This is not News...");
        }
    }

    public function publish() {

    }

    public function delete() {
	$news = News::view($_GET['id']);
        News::delete($_GET['id']);
        require_once('views/news/delete.php');
    }

}
