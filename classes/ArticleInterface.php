<?php

interface ArticleInterface {
    
    public function view($id);
    public function publish(Article $article);
    public function delete($id);

}
