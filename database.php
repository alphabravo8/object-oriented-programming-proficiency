<?php

global $data;

$data = array(
    1 => new News('RockingNews','News Subtitle','News Excerpt','The News Body here'),
    2 => new Review('Review','Review Subtitle','Review Excerpt','The Review Body here'),
    3 => new Feature('Feature','Feature Subtitle','Feature Excerpt','The Feature Body here'),
    4 => new Feature('Another Feature','Another Feature Subtitle','Another Feature Excerpt','Another Feature Body'),
);

