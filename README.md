# TeamRock OOP Proficiency Test
---

Let us imagine that we're building a content management system ...

##Task 1
1. Fork the following repository to your own BitBucket account

##Task 2
1. Create all relevant classes for the entities described below
2. Commit your work with log message "Task 2"

###Entities:

1. News
2. Feature
2. Review

Your entities should have the following properties:

1. Title
2. Subtitle
3. Excerpt
4. Body

## Task 3
1. Add additional properties that you believe to be apt for each of the entities
2. Commit your work with log message "Task 3"

## Task 4
1. Allow content entities to perform the actions described below
2. Commit your work with log message "Task 4"

###Actions:
1. Deleted
2. Published
3. Viewed

## Advice
* Read PSR-1 & PSR-2
* Consider ALL abstractions - overthink it rather than under
* Consider using Interfaces and Traits, when/if applicable
* Consider what being *Viewed* might be/mean within the Model or Controller layer, not the View.

