<?php

require 'classes/Autoloader.php';
Autoloader::register(); 
require 'database.php';

// inc routes...

if (isset($_GET['controller']) && isset($_GET['action'])) {
    
    $controller = $_GET['controller'];
    $action = $_GET['action'];

    switch ($controller) {
        case "news":
            NewsController::$action();
        break;
        case "features":
            FeatureController::$action();
        break;
        case "reviews":
            ReviewController::$action();
        break;
    }

}

